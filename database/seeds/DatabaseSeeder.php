<?php

use App\User;
use App\about;
use App\contact;
use App\project;
use App\languages;
use App\framework;
use App\workExperience;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * 
     * Code needs to refactor, in the mean time this will be used to generate the view
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 5)->create();
        foreach($users as $user) {
            $about = factory(about::class)->create(['user_id' => $user->id]);
            $language = factory(languages::class)->create(['user_id' => $user->id]);
            $frameworks = factory(framework::class)->create(['language_id' => $language->id, 'user_id' => $user->id]);
            $projects = factory(project::class)->create(['user_id' => $user->id]);
            $contact = factory(contact::class)->create(['user_id' => $user->id]);
            $workExp = factory(workExperience::class)->create(['user_id' => $user->id]);
        }
    }
}
