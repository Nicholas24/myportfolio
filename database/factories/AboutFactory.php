<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\about;
use App\User;
use Faker\Generator as Faker;

$factory->define(about::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'description' =>  $faker->paragraph
    ];
});
