<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\contact;
use App\User;
use Faker\Generator as Faker;

$factory->define(contact::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'github' => $faker->url,
        'gitlab' => $faker->url,
        'linekdin' => $faker->url,
        'twitter' => $faker->url,
        'facebook' => $faker->url,
        'instagram' => $faker->url
    ];
});
