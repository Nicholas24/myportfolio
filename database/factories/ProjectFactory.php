<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\project;
use Faker\Generator as Faker;

$factory->define(project::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'url' => $faker->url,
        'github' => $faker->url,
        'gitlab' => $faker->url
    ];
});
