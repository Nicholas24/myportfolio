<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\languages;
use Faker\Generator as Faker;

$factory->define(languages::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => $faker->url,
        'user_id' => factory(User::class)->create()->id
    ];
});
