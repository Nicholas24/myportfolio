<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\languages;
use App\framework;
use Faker\Generator as Faker;

$factory->define(framework::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => $faker->url,
        'language_id' => factory(languages::class)->create()->id,
        'user_id' => factory(User::class)->create()->id
    ];
});
