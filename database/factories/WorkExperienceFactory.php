<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\workExperience;
use Faker\Generator as Faker;

$factory->define(workExperience::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'position' => $faker->jobTitle,
        'start_date' => $faker->date($format = 'Y-m-d', $startDate = '-10 years', $endDate = 'now'),
        'current_date' => $faker->date($format = 'Y-m-d', $startDate = '+10 years')
    ];
});
